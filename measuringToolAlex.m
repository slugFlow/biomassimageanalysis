clc
close all
clear
format short g;
format compact;

%% read the files and save grayscale and binary images in variables just for future reference
baseFileName = 'Straw1.JPG';
folder = [];
fullFileName = fullfile(folder, baseFileName);
originalImage = fullFileName; 
colourImage = imread(fullFileName);
colourImageOriginal = colourImage;
grayImage = rgb2gray(colourImage);
binaryImage = imbinarize(grayImage); 

%% make a binary (masked) image
% measure the hue, saturation and value 
[h,s,v] = rgb2hsv(colourImage);
% make a mask for the blue colour
msk = (h > 0.2 & h < 0.8);
% invert the mask
msk = ~msk;
% not sure what this does, but it does something
colourImage(find(msk(:))+numel(msk)*0) = 0;
colourImage(find(msk(:))+numel(msk)*1) = 0;
colourImage(find(msk(:))+numel(msk)*2) = 0;
% put msk into another variable j ust for the sake of it I guess, could not  be bothered to change msk earlier because that's what the example used
maskedImage = msk;
% show some images
figure
    imshow(cat(2,colourImageOriginal,colourImage));
figure
    imshow(binaryImage)
figure
    imshow(cat(2,maskedImage,msk));

% filter the number of objects down to 4 and draw boxes around them    
maskedImage = bwareafilt(maskedImage, 4);
statsBI = regionprops(maskedImage,'BoundingBox');
%% plot the images with their boxes
figure
    imshow(originalImage)
    for k = 1 : length(statsBI)
        Box = statsBI(k).BoundingBox;
        boxCell{k} = statsBI(k).BoundingBox;
        rectangle('Position', [Box(1), Box(2), Box(3), Box(4)],'LineWidth',3)
    end
    
%% put whatever is in the boxes into separate variables and filter so that we have 4 variables with 4 images with a single object in a single variable
for i = 1:4
    croppedImageCell{i} = imcrop(maskedImage,boxCell{i});
    croppedImageCell{i} = bwareafilt(croppedImageCell{i},1);
end

%% rotate the images simply by angle that is equal to the atan of the length/width of each box
for i = 1:4
    [row,col] = size(croppedImageCell{i});
    angle = atan(row/col);
    angle = angle*180/3.14;
    croppedRotatedImageCell{i} = imrotate(croppedImageCell{i},angle);
end
%% plot the rotated and non rotated images
figure
    subplot(4,1,1);
        imshow(croppedRotatedImageCell{1});
    subplot(4,1,2);
        imshow(croppedRotatedImageCell{2});
    subplot(4,1,3);
        imshow(croppedRotatedImageCell{3});
    subplot(4,1,4);
        imshow(croppedRotatedImageCell{4});  
        
figure
    subplot(4,1,1);
        imshow(croppedImageCell{1});
    subplot(4,1,2);
        imshow(croppedImageCell{2});
    subplot(4,1,3);
        imshow(croppedImageCell{3});
    subplot(4,1,4);
        imshow(croppedImageCell{4});
%% calculate widths, average widths, areas and lengths
for i = 1:4
    width{i} = sum(croppedRotatedImageCell{i}~=0,1);
    % remove widths that are smaller than 5 from the arrays simply to exclude them from the average calculation
    width{i}(width{i} < 5) = [];
    meanWidth{i} = mean(width{i});
    area{i} = sum(croppedRotatedImageCell{i}(:)==1);
    length{i} = area{i}./meanWidth{i};
end
length
meanWidth
