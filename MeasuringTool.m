
%%Import Images
clc;        % Clear the command window.
close all;  % Close all figures 
clear;      % Erase all existing variables.
workspace;          %Make sure the workspace panel is showing.
format short g;     %format output to fix more in display
format compact;
fontSize = 25;
baseFileName = 'Straw1.JPG';  % Input the name of the image
%baseFileName = '1k_100kph_5x20mm.JPG'; 
% Get the full filename, with path prepended.
folder = []; % Determine folder
fullFileName = fullfile(folder, baseFileName);
originalImage = fullFileName; 
%===============================================================================
% Read in image.
grayImage = imread(fullFileName);
% Get the dimensions of the image.
% numberOfColorChannels should be = 1 for a gray scale image, and 3 for an RGB color image.
[rows, columns, numberOfColorChannels] = size(grayImage);
if numberOfColorChannels > 1
	% It's color.
	% Use weighted sum of ALL channels to create a gray scale image.
	 grayImage = rgb2gray(grayImage);
	% ALTERNATE METHOD: Convert it to gray scale by taking only the green 
	% or blue channel	
    % grayImage = grayImage(:, :, 2); % Take green channel.
    % grayImage = grayImage(:, :, 1); % Take blue channel.
end
% Display the image.
subplot(2, 2, 1);
imshow(grayImage, []);
axis on;
axis image;
caption = sprintf('Original Gray Scale Image');
title(caption, 'FontSize', fontSize, 'Interpreter', 'None');
drawnow;
hp = impixelinfo();

%% Set up figure properties: 
%Enlarge figure to full screen.
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0, 1, 1]);
% Get rid of tool bar and pulldown menus that are along top of figure.
% set(gcf, 'Toolbar', 'none', 'Menu', 'none');
% Give a name to the title bar.
set(gcf, 'Name', 'Demo by ImageAnalyst', 'NumberTitle', 'Off')
drawnow;
% binarize the image.
binaryImage = imbinarize(grayImage); 
% Make sure there is n blobs
binaryImage = bwareafilt(binaryImage, 4);
% Display the image.
subplot(2, 2, 2);
imshow(binaryImage, []);
axis on;
axis image;
caption = sprintf('Binary Image');
title(caption, 'FontSize', fontSize, 'Interpreter', 'None');
drawnow;
hp = impixelinfo();
% Label the image
labeledImage = bwlabel(binaryImage);
%% Draw Images

% h =  load_system('ex_vision_remove_noise');
% binaryImagePost = sim('ex_vision_remove_noise.slx', binaryImage(10));
% Make measurements of bounding box

statsBI = regionprops(binaryImage,'BoundingBox');
statsLI = regionprops(labeledImage, 'BoundingBox');

%width = props.BoundingBox(3);
%height = props.BoundingBox(4);
%Display it with the box overlaid on it.
subplot(2, 2, 3);
imshow(originalImage);
%Place Bounding boxes measured from binary image over original image
hold on
for k = 1 : length(statsBI)
    Box = statsBI(k).BoundingBox;
    rectangle('Position', [Box(1), Box(2), Box(3), Box(4)])
end
axis on;
axis image;
caption = sprintf('Original Image with Box Overlaid');
title(caption, 'FontSize', fontSize, 'Interpreter', 'None');
drawnow;
hp = impixelinfo();
subplot(2, 2, 4);
imshow(originalImage);
caption = sprintf('Original Image with bwboundaries()'); 
title(caption, 'FontSize', fontSize, 'Interpreter', 'None');
axis image; % Make sure image is not artificially stretched because of screen's aspect ratio.
hold on;
boundaries = bwboundaries(binaryImage);
numberOfBoundaries = size(boundaries, 1); 

for k = 1 : numberOfBoundaries
	thisBoundary = boundaries{k};
	plot(thisBoundary(:,2), thisBoundary(:,1), 'w', 'LineWidth', 1.5);
end
hold off;

%Print the Sizes
Measurements = regionprops('table',binaryImage,'Centroid', ...
    'MajorAxisLength','MinorAxisLength')
Length = Measurements.MajorAxisLength/7.804%
Width = Measurements.MinorAxisLength/7.804
